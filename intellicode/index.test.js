const fs = require('fs');
const { clearTodoItems } = require('./functions');

describe('clearTodoItems', () => {
  it('should clear the data in storage.json', () => {
    // Mock the fs.writeFile function
    fs.writeFile = jest.fn();

    // Call the function
    clearTodoItems();

    // Check if fs.writeFile was called with the correct arguments
    expect(fs.writeFile).toHaveBeenCalledWith(
      './storage.json',
      '[]',
      'utf8',
      expect.any(Function)
    );
  });

  it('should log a message when data is cleared', () => {
    // Mock the fs.writeFile function
    fs.writeFile = jest.fn();

    // Mock the console.log function
    // console.log = jest.fn();

    // Call the function
    clearTodoItems();

    // Check if console.log was called with the correct message
    expect(console.log).toHaveBeenCalledWith('Data cleared from storage.json');
  });

  it('should handle errors when writing to storage.json', () => {
    // Mock the fs.writeFile function
    fs.writeFile = jest.fn((path, data, encoding, callback) => {
      callback(new Error('Write error')); // Simulate write error
    });

    // Call the function
    clearTodoItems();

    // Check if the error is handled correctly
    // You can customize this assertion based on your error handling logic
    expect(fs.writeFile).toThrow('Write error');
  });
});

it('should return 404 status when trying to delete a non-existent todo', () => {
  // Mock the data to be read from the storage file
  const existingData = JSON.stringify([]);
  fs.readFile.mockImplementation((path, encoding, callback) => {
    callback(null, existingData);
  });

  // Mock the response object
  const res = {
    status: jest.fn().mockReturnThis(),
    send: jest.fn(),
  };

  // Call the function to be tested with a non-existent todo ID
  app.delete('/todos/123', null, res);

  // Verify that the response status and message are as expected
  expect(res.status).toHaveBeenCalledWith(404);
  expect(res.send).toHaveBeenCalledWith('Todo not found');
});

// Import required libraries
const { v4: uuidv4 } = require('uuid');

// Unit test for validating 'id' parameter in deleteTodoById function
it('should validate the id parameter to ensure it is a valid UUID', () => {
  // Define a valid UUID
  const validId = uuidv4();

  // Define an invalid UUID
  const invalidId = 'invalid-uuid';

  // Mock the request object with a valid UUID
  const validReq = { params: { id: validId } };

  // Mock the request object with an invalid UUID
  const invalidReq = { params: { id: invalidId } };

  // Call the deleteTodoById function with the valid request object
  app.delete('/todos/:id', (req, res) => {
    const { id } = req.params;
    expect(uuidv4.validate(id)).toBe(true);
  }, validReq);

  // Call the deleteTodoById function with the invalid request object
  app.delete('/todos/:id', (req, res) => {
    const { id } = req.params;
    expect(uuidv4.validate(id)).toBe(false);
  }, invalidReq);
});

it('should return 500 status when an error occurs while deleting the todo', () => {
  // Mock the data to be read from the storage file
  const existingData = JSON.stringify([]);
  fs.readFile.mockImplementation((path, encoding, callback) => {
    callback(null, existingData);
  });

  // Mock the saveTodos function to simulate an error
  saveTodos.mockImplementation(() => {
    throw new Error('Write error');
  });

  // Mock the response object
  const res = {
    status: jest.fn().mockReturnThis(),
    send: jest.fn(),
  };

  // Call the function to be tested with a valid todo ID
  app.delete('/todos/123', null, res);

  // Verify that the response status and message are as expected
  expect(res.status).toHaveBeenCalledWith(500);
  expect(res.send).toHaveBeenCalledWith('An error occurred');
});

it('should not delete any todos when the id parameter is not a UUID', () => {
  // Mock the data to be read from the storage file
  const existingData = JSON.stringify([{ id: '123e4567-e89b-12d3-a456-426614174000', task: 'Complete assignment' }]);
  fs.readFile.mockImplementation((path, encoding, callback) => {
    callback(null, existingData);
  });

  // Mock the saveTodos function to ensure it is not called
  saveTodos.mockImplementation(() => {
    throw new Error('saveTodos should not be called');
  });

  // Call the function to be tested with a non-UUID id
  app.delete('/todos/invalid-uuid', null, res);

  // Verify that the saveTodos function is not called
  expect(saveTodos).not.toHaveBeenCalled();

  // Verify that the response status is as expected
  expect(res.status).toHaveBeenCalledWith(404);

  // Verify that the response message is as expected
  expect(res.send).toHaveBeenCalledWith('Todo not found');
});