# Django REST и API
```
https://platform.productstar.ru/dd5ed729/33fb4dba-2f70-4b35-81df-c156e32ac592?tab=practice

Задание:
1. Django-проект должен иметь заполненные файлы requirements.in, requirements.txt, .gitignore.
steps:
python -m venv venv
.\venv\Scripts\activate
pip install pip-tools
create file requirements.in (Django==5.1.1, djangorestframework)
pip-compile.exe (generate requirements.txt)
pip-sync.exe (to install only neccessary requirements)
https://docs.djangoproject.com/en/5.1/intro/tutorial01/
django-admin startproject mysite

2. На базе Django REST реализовать возможности:
 • Регистрация пользователя по имейлу и паролю с указанием типа поставщик, потребитель
 • Аутентификация пользователя
 • Создание склада
 • Создание товара
 • Поставщик должен иметь возможность поставлять товар на выбранный склад
 • Потребитель должен иметь возможность забирать товар со склада

python manage.py startapp api

5. Ограничения:
 • Потребитель не может поставлять товар
 • Поставщик не может получать товар
 • Потребитель не может получить товара больше, чем имеется на складе
```



## запуск приложения
```
python manage.py runserver
```

