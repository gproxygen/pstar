from rest_framework.routers import DefaultRouter

from api.views import *

router = DefaultRouter()
router.register('user', UserModelViewSet)
router.register('store', StoreModelViewSet)
router.register('product', ProductModelViewSet)
router.register('trade', TradeModelViewSet)


urlpatterns = [
]

urlpatterns.extend(router.urls)
