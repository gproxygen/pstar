from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    role = models.IntegerField(default=0, choices=((0,'поставщик'),(1,'покупатель')))  

class Store(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.id}: {self.name}"


class Product(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return f"{self.id}: {self.name}"


class Trade(models.Model):
    num = models.IntegerField()
    store = models.ForeignKey(Store, related_name="trades", on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='trades', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='trades', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.store.name}; {self.product.name}; {self.user.name}"
