from rest_framework import serializers
from rest_framework import validators
from django.db.models import Sum

from api.models import User, Product, Store, Trade


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=128, validators=[
        validators.UniqueValidator(User.objects.all())
    ])
    email = serializers.EmailField(validators=[
        validators.UniqueValidator(User.objects.all())
    ])    
    role = serializers.IntegerField(min_value=0, max_value=1)

    password = serializers.CharField(min_length=6, max_length=20, write_only=True)

    def update(self, instance, validated_data):

        if password := validated_data.get("password"):
            instance.set_password(password)
            instance.save(update_fields=["password"])
        return instance

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data["email"],
            username=validated_data["username"],
            role=validated_data["role"] 
        )

        user.set_password(validated_data["password"])
        user.save(update_fields=["password"])
        return user

class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = "__all__"

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"
        
class TradeSerializer(serializers.ModelSerializer): 

    def create(self, body):
        
        user_id = body["user"].id
        store_id = body["store"].id
        product_id = body["product"].id        
        sum = Trade.objects.filter(store__id=store_id, product__id=product_id).aggregate(Sum('num'))['num__sum']   
        balance = sum if sum else 0
        print(balance)
        num = body["num"]   
                      
        if (body["user"].role == 1): # потребитель
            if num > -1: # забирает товар?
                raise ValueError('Покупатель не может приносить товар')
            if (balance+num<0):  # хватает товара?
                raise ValueError(f'Всего "{body["product"].name}", на "{body["store"].name}" {balance}, нельзя выдать {-num}')
        else:
            if num < 1: # принес что-нибудь?
                raise ValueError('Поставщик не может забирать товар')
                    
        trade = Trade.objects.create(user_id=user_id,store_id=store_id,product_id=product_id,num=num)
        return trade

    class Meta:
        model = Trade
        fields = "__all__"
