import { useState } from "react";
import Gallery from "./components/Gallery";


function App() {

  const [index, setIndex] = useState(0);

  return (
    <Gallery
      images={[
        'images/1.png',
        'images/2.png',    
        'images/3.png',
        'images/4.png',            
        'images/5.png',
        'images/6.png',            
      ]}  
      index = {index}
      onRandom = { () => {        
        setIndex(Math.round(Math.random()*5))      
      }}      
    />
  );
}

export default App;
