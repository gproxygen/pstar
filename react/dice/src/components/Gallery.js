import "./Gallery.css";

export default function Gallery({images, index, onRandom}) {

    return ( 
    <div className = "wrapper">        
        <img 
            key= {index}            
            src = {images[index]}
        />

        <button onClick={onRandom}>Бросить кубик</button>

    </div>
    )
}