# Приветствие
echo "Hello World!"

# Cпрашивать у пользователя последовательно имя, фамилию и выводить приветствие «Hello, $Name, $Last_Name!!!»
read -p "Enter your Name: " Name
read -p "$Name, enter your surname: " Last_Name
echo "Hello $Name $Last_Name, nice to meet you!"

# Условие. Запросить возраст пользователя, вывести "you so young" если менее 20 лет
read -p "age= " age
if [ $age -lt 20]; then
    echo "you so young";fi

# дополнение
if [ $a -lt $b ]; then
    echo "a<b"
elif [ $a == $b ]; then
    echo "a=b"
else
    echo "a>b";fi

# Циклы. Запросить число, вывести сумму чисел от 1 до указанного числа (for, while, until)
read -p "N= " END
i=1;sum=0

while [[ $i -le $END ]]; do
  ((sum = sum + i));((i++))
done
echo "*WHILE, sum = $sum"

sum=0
for i in $(seq 1 $END); do
  ((sum = sum + i))
done
echo "*FOR, sum = $sum"

i=1;sum=0
until [ $i -gt $END ]; do
  ((sum = sum + i)); ((i++))
done
echo "*UNTIL, sum = $sum"

# Файлы. Вывести список файлов в текущем каталоге, в содержании которых есть слово "key"
for file in ./*
do
  if grep -q -d skip "key" $file
  then
    echo $file
  fi
done