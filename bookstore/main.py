from flask import Flask, jsonify, request
from model.twit import Twit

app = Flask(__name__)
twits = []            

# чтение
@app.route('/', methods=['GET'])
def twit_list():     
   return jsonify({'twits': [twit.to_json() for twit in twits]})

# создание
@app.route('/', methods=['POST'])
def twit_create():
    twit_json = request.get_json()       
    id = twits[-1].to_json()["id"]+1 if twits else 1
    twit = Twit(id, twit_json['body'],twit_json['author'])
    twits.append(twit)
    return {'success': True, 'twit': twit.to_json()}

# удаление
@app.route('/<id>', methods=['DELETE'])
def twit_delete(id):     
   for i in range(len(twits)):
       if  twits[i].to_json()["id"] == int(id):
            twits.remove(twits[i])
            return {'success': True}            
   return {'success': False}

# редактирование
@app.route('/<id>', methods=['PUT'])
def twit_update(id):     
   twit_json = request.get_json()
   for twit in twits:       
       if  twit.to_json()["id"] == int(id):
            print('YESSSSS')
            twit.body = twit_json['body']
            twit.author = twit_json['author']
            return {'success': True}            
   return {'success': False}

if __name__ ==  '__main__':
    app.run(debug=True)