from flask import Flask
from context import Context
from views.book import bp as book_bp

from domain.book_model import db


def create_app():
    app = Flask(__name__)
    app.app_context().push()

    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///my_database.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)
    db.create_all()

    app.register_blueprint(book_bp, url_prefix="/books")
    app.config["CONTEXT"] = Context()
    return app


app = create_app()
