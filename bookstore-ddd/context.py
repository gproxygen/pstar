from application.book_service import BookService
from infra.storage.storage_sqlite import SQLiteStorage
from domain.book_model import db


class Context:
    def __init__(self):
        book_storage = SQLiteStorage(db)
        self.book_service = BookService(book_storage)


def get_context(app):
    return app.config["CONTEXT"]
