from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class BookModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    publish_year = db.Column(db.Integer, nullable=False)
    pages_count = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f"<Book {self.id} = {self.title}>"
