from abc import ABC, abstractmethod

from domain.book_model import BookModel


class BookRepository(ABC):
    @abstractmethod
    def create(self, book: BookModel):
        pass

    @abstractmethod
    def read(self, book_id: int):
        pass

    @abstractmethod
    def update(self, book_id: int, book: BookModel):
        pass

    @abstractmethod
    def delete(self, book_id: int):
        pass

    @abstractmethod
    def list(self):
        raise NotImplementedError()
