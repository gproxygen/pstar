class BookService:
    def __init__(self, storage):
        self.storage = storage

    def create(self, book):
        return self.storage.create(book)

    def read(self, id):
        return self.storage.read(id)

    def update(self, id, book):
        return self.storage.update(id, book)

    def delete(self, id):
        return self.storage.delete(id)

    def list(self):
        return self.storage.list()
