import json

from flask import Blueprint, current_app, request

from context import get_context
from domain.book_model import BookModel

bp = Blueprint("book", __name__)


def book_to_dict(book: BookModel):
    return {
        "id": book.id,
        "title": book.title,
        "description": book.description,
        "publish_year": book.publish_year,
        "pages_count": book.pages_count,
    }


@bp.route("/", methods=["POST"])
def create_book():
    ctx = get_context(current_app)
    book = BookModel(**request.json)
    book_id = ctx.book_service.create(book)
    return {"id": book_id, "book": book_to_dict(book)}

@bp.route("/<id>", methods=["GET"])
def read_book(id):
    ctx = get_context(current_app)
    return book_to_dict(ctx.book_service.read(id))


@bp.route("/<id>", methods=["PUT"])
def update_book(id):
    ctx = get_context(current_app)
    book = BookModel(**request.json)
    data = ctx.book_service.update(id, book)
    return book_to_dict(data)

@bp.route("/<id>", methods=["DELETE"])
def delete_book(id):
    ctx = get_context(current_app)
    ctx.book_service.delete(id)
    return {}

@bp.route("/")
def list_book():
    ctx = get_context(current_app)
    return json.dumps([book_to_dict(book) for book in ctx.book_service.list()])
