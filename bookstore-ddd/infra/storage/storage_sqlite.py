from sqlalchemy.orm import sessionmaker

from application.book_repository import BookRepository
from domain.book_model import BookModel


class SQLiteStorage(BookRepository):
    def __init__(self, db):
        self.db = db
        self.session = sessionmaker(bind=self.db.engine)()

    def create(self, book: BookModel):
        self.session.add(book)
        self.session.commit()
        return book.id

    def read(self, book_id: int):  
        return self.session.query(BookModel).filter(BookModel.id == book_id).first() 

    def update(self, book_id: int, book: BookModel):
        v = self.session.query(BookModel).filter(BookModel.id == book_id).first()
        if not v:
            raise IndexError
        v.title = book.title
        v.description = book.description
        v.publish_year = book.publish_year
        v.pages_count = book.pages_count
        self.session.commit()
        print(v.title)
        return v

    def delete(self, book_id: int):
        self.session.query(BookModel).filter(BookModel.id == book_id).delete()
        self.session.commit()

    def list(self):
        return self.session.query(BookModel).all()
