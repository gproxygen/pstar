class MemoryStorage:
    def __init__(self):
        self.books = []

    def create(self, book):
        self.books.append(book)
        return len(self.books) - 1

    def delete(self, id):
        del self.books[int(id)]

    def list(self):
        return self.books
