# Архитектура и серверная часть, сервис для работы с книгами (flask, ddd)
```
https://platform.productstar.ru/dd5ed729/a27566a8-ee35-4055-b807-00ab7e0d75a2?tab=practice
```
## Задание:

Реализовать поддержку SQLite, при этом следовать DDD и взаимодействовать с БД через паттерн Repository. Основная идея паттерна Repository заключается в предоставлении промежуточного слоя между доменной моделью и слоями хранения данных, что позволяет доменной модели не зависеть от деталей реализации доступа к данным.

## Требования:

Для реализации паттерна Repository требуется реализовать следующие объекты:
- класс BookRepository, в котором будут абстрактные методы для добавления, чтения, обновления и удаления объектов из хранилища данных (CRUD - create, read, update, delete — операции для манипуляции с данными);
- класс SqlLiteBookRepository, который будет наследником класса BookRepository, и в котором будут реализованы соответствующие CRUD методы;
для реализации CRUD методов с SQLite лучше использовать библиотеку flask-sqlachemy,


## создание и активация виртуальной среды (если еще не настроена), перейти на каталог выше и выполнить команды:
```
python -m venv venv
venv/Scripts/activate || source venv\bin\activate
pip install -r .\requirements.txt
```

## запуск приложения

```
flask --app main --debug run
```

## cURL тестирование

### создание книги
```
curl -XPOST "localhost:5000/books/" --json "{\"title\": \"book1\", \"description\":\"descr1\",\"publish_year\": 2023, \"pages_count\": 192}"

{
  "book": {
    "description": "descr1",
    "pages_count": 192,
    "publish_year": 2023,
    "title": "book1"
  },
  "id": 0
}
```

### прочитать по id
```
curl "localhost:5000/books/1"
```

### отредактировать по id
```
curl -XPUT "localhost:5000/books/1" --json "{\"title\": \"Изменили название\", \"description\":\"222\",\"publish_year\": 2000, \"pages_count\": 123}"
```

### удалить по id
```
curl -XDELETE "localhost:5000/books/0"
{}
```

### получить все записи
```
curl "localhost:5000/books/"
```
