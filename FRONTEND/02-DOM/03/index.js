let time = 100;
let timerId;
let loadingCount = 4;

function updateTimer() {
  timerId = setTimeout(updateTimer, 25);
  time -= 0.5;
  if (time <= 0) {
    refresh()
  }
  document.querySelector(".progress").style.width = time + "%";
}

function refresh() {
  loadingCount = 4;
  clearTimeout(timerId);
  time = 100;
  document.querySelector(".progress").style.width = time + "%";
  document.querySelectorAll("img").forEach((image, i) => {
    fetch("https://random.imagecdn.app/v1/image?width=600&height=400&category=nature&format=json")
      .then(response => response.json())
      .then(data => {
        image.src = data.url
        image.classList.add("loading")
        image.nextElementSibling.textContent = data.provider + ', license: ' + data.license
      })
  })
}

function loaded(event) {
  loadingCount -= 1;
  if (loadingCount === 0) {
    updateTimer()
  }
  event.target.classList.remove("loading");
}

function zoomIn(event) {
  clearTimeout(timerId);
  if (event.target.tagName === "IMG") {
    const parent = event.target.parentElement;
    parent.classList.toggle("fullScreen");
  }
  if (event.target.classList.contains("fullScreen")) {
    event.target.classList.toggle("fullScreen")
  }
}


function init() {
  refresh();
  document.querySelector(".nextButton").onclick = refresh;
  document.querySelector(".container").addEventListener('click', zoomIn);
  document.querySelectorAll("img").forEach((image) => {
    image.onload = loaded;
  })
}

window.addEventListener("DOMContentLoaded", init)