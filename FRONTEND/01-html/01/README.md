# Структура веб страницы, базовый HTML
```
Задание:
1. Создать HTML-страницу с галереей.
2. Добавить ссылку на генерацию новых изображений с любого доступного вам сайта генератора изображений (например, dummyimage.com, loremflickr.com, placeit.net)
3. Добавить подпись автора.
4. Прислать ссылку на проект и на превью (codesandbox.io, вход через github)

https://codesandbox.io/p/sandbox/vmk6f3