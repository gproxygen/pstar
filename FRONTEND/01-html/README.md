### Принципы HTML

# 01 Структура веб страницы, базовый HTML (структура страницы)
https://platform.productstar.ru/dd5ed729/8c67261e-7145-496a-89dd-e2a54ca36a7d?tab=intro

# 02 Базовый CSS, селекторы и каскад (подключение css)
https://platform.productstar.ru/dd5ed729/6495eca3-8d9c-44d7-ba59-84b9d49a6e6f?tab=intro

# 03 Продвинутый CSS, адаптивность и кроссбраузерность (flex-верстка)
https://platform.productstar.ru/dd5ed729/191c5504-f3fd-41d6-be69-dbca28ae4b78?tab=intro

# 04 Основы JavaScript, инструменты для разработки и отладки (тепло или холодно на улице)
https://platform.productstar.ru/dd5ed729/87a86b2f-0f0e-441f-a1bb-41340f76578f?tab=intro