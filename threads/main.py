import threading
import time
# Многопоточность
# https://platform.productstar.ru/dd5ed729/dc8bdd94-bef1-430a-8c61-a9ff4c37b3ec?tab=class
class Autoservice:    
    """ Автосервис с двумя подъемниками. для осуществления ремонта подъезжают машины 
    и выстраиваются в очередь, но одновременно не может ремонтироваться более 3 
    автомобилей. Один завершен - поднимается другой """
    
    def __init__(self):
        self.client_count = threading.Semaphore(value=3)

    def accept_client(self, client):
        
        print(f'Приглашаем клиента {client}')
        self.client_count.acquire()

        print(f'Обслуживание клиента {client}')
        time.sleep(4)

        print(f'==Завершение работы с клиентом {client}')
        self.client_count.release()

    def clients(self, count):

        for client in range(count):
            client_thread = threading.Thread(target=self.accept_client, args=[client+1])
            client_thread.start()


if __name__ == '__main__':
    Clients = Autoservice()
    Clients.clients(6)
