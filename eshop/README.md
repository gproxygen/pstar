# Интернет-магазин 
```
https://platform.productstar.ru/dd5ed729/43684b26-186f-40c0-8e97-bde7912ca9d6?tab=practice

Задание:
Доработать Интернет-магазин воркшопа:
1. API для создания продукта;
2. API для получения страницы продуктов;
3. API для получения продукта по его ID;
4. README.md с описанием:
 — как запустить сервер;
 — как создать продукт;
 — как получить все продукты;
 — как получить продукт по id.
```

## создание и активация виртуальной среды (при необхлдимости):
```
python -m venv .venv
.venv/Scripts/activate
pip install flask
pip install marshmallow
```

## запуск приложения

```
python ./main.py run
```


### создание продукта
```
curl -X POST "localhost:5000/api/v1/product" -H "Content-Type: application/json" -d "{\"name\": \"Orange juice\", \"price\": 23.22}" 

{"id":"4","name":"Orange juice","price":23.22}
```

### получить продукт по id
```
curl "localhost:5000/api/v1/product/4"
{
  "id": "4",
  "name": "Orange juice",
  "price": 23.22
}
```

### получить все продукты
```
curl "localhost:5000/api/v1/product?page=1&limit=2"
[
  {
    "id": "3",
    "name": "Ноутбук",
    "price": 12.0
  },
  {
    "id": "4",
    "name": "Orange juice",
    "price": 23.22
  }
]
```


