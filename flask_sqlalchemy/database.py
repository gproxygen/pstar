import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class Genre(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True) 
    name = db.Column(db.String)
    description = db.Column(db.String, nullable=True)
    books = relationship("Book", back_populates="genre")  

    def __repr__(self):
        return f"Genre(name={self.name!r})"


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)    
    publication_date = db.Column(db.DateTime, default=datetime.datetime.now())
    title = db.Column(db.String) 
    author = db.Column(db.String)
    annotation = db.Column(db.String, nullable=True)
    is_read = db.Column(db.Boolean, default = False)
    
    genre_id = db.Column(db.Integer, db.ForeignKey('genre.id'))
    genre = relationship("Genre", back_populates="books")
    
    def __repr__(self):
        return f"Book(title={self.name!r})"




