#!/usr/bin/env python

import os

from flask import Flask, request, redirect
from flask import render_template
from sqlalchemy import desc

from database import db, Book, Genre

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///../sqlite.db" #//os.getenv("SQLALCHEMY_DATABASE_URI")
db.init_app(app)


with app.app_context():
    # pass
    db.drop_all()
    db.create_all()
    for i in range(1,20):
        db.session.add(Book(
            title = f"Книга №{i}",
            author = f"Автор №{i%7+1}",
            genre_id = i%3+1
        ))
    for i in range(1,4):
        db.session.add(Genre(name = f"Жанр №{i}"))
    db.session.commit()


@app.route("/")
def all_books():    
    books = Book.query.order_by(desc("id")).limit(15)
    return render_template("all.html", books=books)


@app.route("/genre/<int:genre_id>")
def books_by_genre(genre_id):
    genre = Genre.query.get_or_404(genre_id)
    return render_template(
        "filtered.html",
        genre_name=genre.name,
        books=genre.books,
    )


@app.route("/book/<int:book_id>", methods=['POST'])
def update_book(book_id):    
    book = db.get_or_404(Book, book_id)
    old_status = book.is_read
    is_read = request.form.get('is_read', None)        
    book.is_read = is_read is not None
    db.session.commit()    

    print("id",book_id,"old_status",old_status, 'new_status:', book.is_read)
    return redirect("/")


if __name__ == '__main__':
    app.run()
