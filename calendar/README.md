# Сервис для работы с Календарем
```
Требования:
— API интерфейс CRUD — Добавление / Список / Чтение / Обновление / Удаление
— модель данных "Событие": ID, Дата, Заголовок, Текст
— локальное хранилище данных
— максимальная длина заголовка — 30 символов
— максимальная длина поля Текст — 200 символов
— нельзя добавить больше одного события в день
— API интерфейс: /api/v1/calendar/…
— формат данных: "ГГГГ-ММ-ДД|заголовок|текст"
```

## создание и активация виртуальной среды (если еще не настроена), перейти на каталог выше и выполнить команды:
```
python -m venv .venv
.venv/Scripts/activate
pip install flask
```

## запуск приложения

```
flask --app ./server.py run
```

## cURL тестирование

### добавление нового события
```
curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "2024-07-26|title|text"
new id: 1

curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "2024-07-29|title29|text29"
new id: 2
```

### добавление нового события с неверными данными
```
curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "2024-07-26|title|text"
failed to CREATE with: failed READ_BY_DATE operation with: 2024-07-26 found in storage

curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "2024-07-266|title|text"
failed to CREATE with: wrong date: 2024-07-266

curl http://127.0.0.1:5000/api/v1/calendar/ -X POST -d "2024-07-28|1234567890123456789012345678901|text"
failed to CREATE with: title lenght > MAX: 30
```

### получение всего списка событий
```
curl http://127.0.0.1:5000/api/v1/calendar/
1|2024-07-26|title|text
2|2024-07-29|title29|text29
```

### получение события по идентификатору / ID == 1
```
curl http://127.0.0.1:5000/api/v1/calendar/1/
1|2024-07-26|title|text
```

### обновление данных о событии по идентификатору / ID == 1 /  новый текст == "new text"
```
curl http://127.0.0.1:5000/api/v1/calendar/1/ -X PUT -d "2024-07-26|заголовок первого события|отправь файл на доработку!"
updated
```

### удаление события по идентификатору / ID == 2
```
curl http://127.0.0.1:5000/api/v1/calendar/2/ -X DELETE
deleted
```

### получение всего списка событий
```
curl http://127.0.0.1:5000/api/v1/calendar/
1|2024-07-26|заголовок первого события|отправь файл на доработку!
```

### удаление всех событий
```
curl http://127.0.0.1:5000/api/v1/calendar/2/ -X DELETE
failed to DELETE with: failed DELETE operation with: failed DELETE operation with: 2 not found in storage

curl http://127.0.0.1:5000/api/v1/calendar/1/ -X DELETE
deleted
```

### получение всего списка событий (пусто т.к. произвели удаление)
```
curl http://127.0.0.1:5000/api/v1/calendar/

```