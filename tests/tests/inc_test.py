import unittest
from parameterized import parameterized
from app.main import Calculator

# основание а больше нуля и не равно единице, а также показатель b больше нуля

class TestLog(unittest.TestCase):
    def setUp(self) -> None:
        self.calc = Calculator()

    def tearDown(self) -> None: 
        ...

    @parameterized.expand(
        # 1. arrange
        [
            ("valid_integer", 3, 4),
            ("valid_float", 1.5, 2.5),
            ("invalid_string", '10', TypeError),
            # ("invalid_None", None, TypeError),
        ]
    )
    def test_types(self, name, a, expected_result):
        # 2. act
        actual_result = self.calc.inc(a)

        # 3. assert
        self.assertEqual(actual_result, expected_result)

if __name__ == "__main__":
    unittest.main()
