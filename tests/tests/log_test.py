import math
import unittest
from parameterized import parameterized
from app.error import InvalidInputException
from app.main import Calculator


# основание а больше нуля и не равно единице, а также показатель b больше нуля


class TestLog(unittest.TestCase):
    def setUp(self) -> None:
        self.calc = Calculator()

    def tearDown(self) -> None: 
        ...

    @parameterized.expand(
        # 1. arrange
        [
            ("valid_integers", 8, 2, 3),
            ("invalid_type", "hello", 2, TypeError),
            ("invalid_input", 10, 0, InvalidInputException),
        ]
    )
    def test_integers(self, name, a, b, expected_result):
        # 2. act
        actual_result = self.calc.log(a, b)

        # 3. assert
        with self.assertRaises(expected_result):
            self.assertEqual(actual_result, expected_result)


if __name__ == "__main__":
    unittest.main()
